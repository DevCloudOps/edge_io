SHELL := /bin/bash
CWD := $(shell pwd)

CFLAGS := -g -O0 -fPIC
CC := gcc -std=c17 $(CFLAGS)
CPP := g++ -std=c++17 $(CFLAGS)
LIBS := -lm -lc

HDR := edgeio.h
SRC := edgeio.c
EXE := edgeio cpp_edgeio
$(info shell: $(SHELL) ... pwd: $(CWD) ... compile and link: $(CC) $(CPP) $(LIBS) $(EXE))

ifndef SWIG
  SWIG := $(shell swig -version)
endif

ifndef PY3INC
  PY3 := $(shell python3 -c 'import sys ; vvv=str(sys.version_info.major)+"."+str(sys.version_info.minor)+"."+str(sys.version_info.micro);print(vvv)')
  PY3INC := /opt/conda3/include/python3.9
endif

#all: info $(EXE) test
#all: clean edgeio cpp_edgeio py3swig test
all: info clean edgeio cpp_edgeio test
	-@echo made all testable targets and ran each
info:
	-@gcc --version ; g++ --version
	-@egrep -i 'name|release' /etc/os-release
#	-@swig -python -help|tail -28

edgeio:
	-@echo $(CC) -c $@.c
	$(CC) -c $@.c
	-@echo $(CC) $@.c -o $@ $(LIBS) 
	$(CC) $@.c -o $@ $(LIBS) 
	ldd $@

# try compile C src using c++
cpp_edgeio:
	-@echo \ln -s edgeio.c $@.cc
	touch $@.cc && rm $@.cc && \ln -s edgeio.c $@.cc
	-@echo $(CPP) $@.cc -o $@  $(LIBS)
	$(CPP) $@.cc -o $@  $(LIBS)
	ldd $@

test:
	-@echo valgrind test C compiled edgeio 
	valgrind -s --leak-check=full ./edgeio
#	-@echo test C++ compiled edgeio++
#	valgrind -s --leak-check=full ./cpp_edgeio
#	valgrind -s --leak-check=full ./cpp_edgeio
#       -@echo test python import if edgeio.so module
#	-@echo ./edgeio.py

py3swig: clean
	@$(info $(SWIG) python3)
	@$(info invoke swig -python3 on a header file that has a module declaration to creates .py and _wrap.c module files)
	@$(info *.h and *.c are supplemented by swig generated files *_wrap.c and *.py)
	@$(info the resulting shared object *.so provides the low-level python module shared library code)
	swig -python $(HDR)
	$(CC) -fPIC -c edgeio.c edgeio_wrap.c -I$(PY3INC) 
	ld -shared edgeio.o edgeio_wrap.o -o edgeio.so.$(PY3) 
	file edgeio.so.$(PY3)
	-@echo python3 -c 'import edgeio'

clean:
	@$(info swig module edgeio.py should be regenerated) 
	-rm -rf $(EXE) *_wrap.* *.o *.so *.so.* *.pyc __pycache__

realclean: clean
	-@echo $(PY3)
	-@echo reset python virtualenv if in use \(TBD\)

