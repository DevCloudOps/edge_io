#if !defined(__edgeio_h__)
#define __edgeio_h__  __FILE__ // "$Name$ $Id$"
#define __edgeio__(arg) const char arg##edgeio_h__Id[] = __edgeio_h__;
static const char __version_edgeio_h[] = "0.0.0";

// allow the use in c++ code ... ala /usr/include
#ifdef __cplusplus
extern "C" {
#endif
 
#ifdef SWIG
%module cedgeio 
#%module edgeio 
%{
#define SWIG_FILE_WITH_INIT
#include "edgeio.h"
%}
#endif

#include "edgeio.h"
//#include "logger.h"
#include "dirent.h"
#include "errno.h"
#include "fcntl.h"
#include "limits.h"
#include "math.h"
#include "linux/nfs4.h"
#include "time.h"
#include "termio.h"
#include "signal.h"
#include "stdbool.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "strings.h"
#include "sys/uio.h"
#include "sys/stat.h"
#include "unistd.h"

extern int edgeio(const int parm);
extern int* busiestServers(int k, int* arrival, int arrivalSize, int* load, int loadSize, int* returnSize);
#ifdef __cplusplus
}
#endif // support c++ compilers
#endif // edgeio.h -- prevents recursive/multiple include

