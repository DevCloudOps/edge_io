// refactored into fewer funcs at the request of the boss :)

//# edge_io
//
//edge.io career advancement C code sample
//
//############################################## begin statement of task ######################################################
//
//Here is the tech assessment. You only need to provide me with the function (no main function needed) in C. 
//
//Thanks,
//Gaurav
//
//You have k servers numbered from 0 to k-1 that are being used to handle multiple requests simultaneously. Each server has infinite computational capacity but cannot handle more than one request at a time. The requests are assigned to servers according to a specific algorithm:
//The ith (0-indexed) request arrives.
//If all servers are busy, the request is dropped (not handled at all).
//If the (i % k)th server is available, assign the request to that server.
//Otherwise, assign the request to the next available server (wrapping around the list of servers and starting from 0 if necessary). For example, if the ith server is busy, try to assign the request to the (i+1)th server, then the (i+2)th server, and so on.
//You are given a strictly increasing array arrival of positive integers, where arrival[i] represents the arrival time of the ith request, and another array load, where load[i] represents the load of the ith request (the time it takes to complete). Your goal is to find the busiest server(s). A server is considered busiest if it handled the most number of requests successfully among all the servers.
//Return a list containing the IDs (0-indexed) of the busiest server(s). You may return the IDs in any order.
// 
//Example 1:
//Input: k = 3, arrival = [1,2,3,4,5], load = [5,2,3,3,3] 
//Output: [1] 
//Explanation: 
//All of the servers start out available.
//The first 3 requests are handled by the first 3 servers in order.
//Request 3 comes in. Server 0 is busy, so it's assigned to the next available server, which is 1.
//Request 4 comes in. It cannot be handled since all servers are busy, so it is dropped.
//Servers 0 and 2 handled one request each, while server 1 handled two requests. Hence server 1 is the busiest server.
//Example 2:
//Input: k = 3, arrival = [1,2,3,4], load = [1,2,1,2]
//Output: [0]
//Explanation: 
//The first 3 requests are handled by first 3 servers.
//Request 3 comes in. It is handled by server 0 since the server is available.
//Server 0 handled two requests, while servers 1 and 2 handled one request each. Hence server 0 is the busiest server.
//Example 3:
//Input: k = 3, arrival = [1,2,3], load = [10,12,11]
//Output: [0,1,2]
//Explanation: Each server handles a single request, so they are all considered the busiest.
// 
//Constraints:
//1 <= k <= 10^5
//1 <= arrival.length, load.length <= 10^5
//arrival.length == load.length
//1 <= arrival[i], load[i] <= 10^9
//arrival is strictly increasing.
//
///**
// * Note: The returned array must be malloced, assume caller calls free().
// */
//int* busiestServers(int k, int* arrival, int arrivalSize, int* load, int loadSize, int* returnSize){}
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

// useful C dev incs
// allow the use in c++ code ... ala /usr/include
#ifdef __cplusplus
extern "C" {
#endif
//#include "logger.h"
#include "dirent.h"
#include "errno.h"
#include "fcntl.h"
#include "limits.h"
#include "math.h"
#include "linux/nfs4.h"
#include "time.h"
#include "termio.h"
#include "signal.h"
#include "stdbool.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "strings.h"
#include "sys/uio.h"
#include "sys/stat.h"
#include "unistd.h"
#ifdef __cplusplus
}
#endif // support c++ compilers

// should we want to use qsort() on int indices lists
int cmpint (const void * a, const void * b) {
  // stdlib qsort int compare
  return ( *(int*)a - *(int*)b );
}

// memory leaks in long-running apps will eventually trigger hep exhaustion!
int* int_alloc(size_t cnt) {
  int* heap = (int*) calloc(cnt, sizeof(int)); // ensure ivals init to 0
  if( ! heap ) { printf("exhausted heap... %s\n", strerror(errno)); }
  return heap;
}

int* busiestServers(int k, int* arrival, int arrivalSize, int* load, int loadSize, int* returnSize) {
  // the context of this func is a tad ambiguous -- if reentrant, then no static vars can be used...
  // need to inspect busy server info/level to assign requests or drop
  // keep track of the server(s) with the least and most loads
  // given k, the total number of servers -- indices:  0 .... (k-1)
  // first deal with params sanity checks and simplesti use cases, then the more general cases
  // arrival list and current server load list are providd 
  // return list of "busiest servers" == meaning the servers assigned to handle the most requests
  // if each server handles the same number of requests, return list of all server (cnt == k)
  // if some servers do not handle any requests, exclude those from the return list
  // load[i] is the time consumed servicing ith request 

  const int maxsz = 100000;
  int* result = NULL;
  int* heap = NULL;
  int busy_cnt = k; // default to k server cnt

  // sanity check params quit or perhaps truncate to maxsz?
  if( k < 1 || k > maxsz ) {
    printf("k param is outa bounds: %d\n", k);
    return NULL;
  }
  if( loadSize < 1 | arrivalSize < 1 || arrivalSize > maxsz ) {
    printf("arravalSize  param is outa bounds: %d\n", arrivalSize);
    return NULL;
  }
  if( !arrival || !load || !returnSize ) {
    printf("please provide valid function params!\n");
    return NULL;
  }
  // inspect params
  printf("parms k, arrivalSize, loadSize: %d, %d, %d\n", k, arrivalSize, loadSize);
  printf("index, arrival[index], load[index]\n");
  for(int ia = 0; ia < arrivalSize; ++ia) {
    printf("arrival: %d, %d\n", ia, arrival[ia]);
  }
  for(int ild = 0; ild < loadSize; ++ild) {
    printf("%d, %d, %d\n", ild, arrival[ild], load[ild]);
  }

  printf("parms k, arrivalSize, loadSize: %d, %d, %d\n", k, arrivalSize, loadSize);

  // singular special case
  if( loadSize == 1 ) {
    heap = int_alloc(loadSize); if( ! heap ) return NULL;
    result = heap; // potential memory leak. invocation code shall be expected to manage this heap alloc..
    // return single element array
    *returnSize = 1;
    *result = 0;
    return result;
  }

  // if arrival list length == k, simply return list of k servers as indicaterd in example 3:...
  if( arrivalSize == k ) {
    heap = int_alloc(k); if( ! heap ) return NULL;
    result = heap; // potential memory leak. invocation code shall be expected to manage this heap alloc..
    *returnSize = k;
    for(int ik = 0; ik < k; ++ik) {
      result[ik] = ik;
    }
    return &result[0];
  }

  //////////////////////   if we get here, deal with general servers nultiplex (mux) cases  /////////////////////////////////

  // sanity check: use qsort to inspect load info.., and be sure to free heap alloc before func returns
  heap = int_alloc(loadSize); if( ! heap ) return NULL;
  int* sorted_load = int_alloc(loadSize); // be sure to free this before func returns
  for(int i = 1; i < loadSize; ++i) { sorted_load[i] = load[i]; } // inline is faster than func context switch using memcpy()
  qsort(sorted_load, loadSize, sizeof(int), cmpint); // ascendinbg vals ... TBD check for duplicates
  int min_ld = sorted_load[0];
  int max_ld = sorted_load[loadSize-1];
  // info level logging:
  for(int i = 1; i < loadSize; ++i) { printf("i, sorted_load[i]: %d, %d\n", i, sorted_load[i]); }
  printf("server index with min load, max load: %d, %d\n", min_ld, max_ld);

  // assign requests to least loaded servers or drop
  // keep track of number of requests assigned to each server
  // if 1 server has the largets count of assigned requests, return it as single item list
  // if 2 or more servers have equally large assignments, return list of those 2 or more
  heap = int_alloc(k); if( ! heap ) return NULL;
  int* busy = heap; // init busy to req_cnt of 0, then increment as needed

  // TBD: optimize by using fewer loops, but non-optimized code is easier to debug, etc.

  int req_ld, next_req_ld = 0;
  // loop over length of arrival list and k servers taking into account req loads 
  for(int req = 0; req < arrivalSize; ++req) {
    req_ld = load[req]; // be mindful of array access
    next_req_ld = (req < arrivalSize-1) ? load[req+1] : load[req]; // re-use final element to stay within bounds 
    printf("req_ld, next_req_ld, min and max loads: %d, %d, %d, %d\n", req_ld, next_req_ld, min_ld, max_ld);
    int srv_idx = req % k;
    if(req_ld > next_req_ld) {
      printf("srv_idx %d is busy, try next idx %d\n", srv_idx, next_req_ld);
      continue;
    }
    busy[srv_idx] += 1; // this implictly assignes req to server[idx] by incrementing its busy cnt
  }

  // evaluate which server(s) have been dealt the greatest number of requests and return its index or list of indices
  // the arrival request with the greater load values should be assigned to the least busy server somehow 
  // worst case scenario is all servers have equal req cnts (and equal loads)
  int max_req_idx, max_req_cnt = 0;
  int max_req = busy[0];
  int* max_case_list = (int*) malloc(k*sizeof(int)); // potential memory leak
  for(int idx = 1; idx < arrivalSize; ++idx) {
    if(busy[idx] >= max_req) {
      max_req_idx = idx; max_req = busy[idx];
    }
  }

  heap = int_alloc(k); if( ! heap ) return NULL;
  int* sorted_busy = heap; // init busy to req_cnt of 0, then increment as needed
  for(int i = 1; i < loadSize; ++i) { sorted_busy[i] = busy[i]; } // inline is faster than func context switch using memcpy()
  qsort(sorted_busy, k, sizeof(int), cmpint); // ascendinbg vals ...

  int idx_max_busy = sorted_busy[k-1]; // shgould be idx of busiest server

  int cnt_busiest = 0;
  for(int idx = k-1; idx >= 0; --idx) {
    if(sorted_busy[idx] >= idx_max_busy) ++cnt_busiest; 
  }
  // list of busiest servers
  heap = int_alloc(cnt_busiest); if( ! heap ) return NULL;
  result = heap; // potential memory leak. invocation code shall be expected to manage this heap alloc..
  for(int idx = 0; idx < cnt_busiest; ++idx) { result[idx] = sorted_busy[k-idx-1]; }    

  // avoid memory leaks:
  free(sorted_load); free(busy); free(sorted_busy);
  return &result[0];
}

int main(int argc, char** args) {
  // const int parm = 0;
  // int result = edgeio(parm);
  // test all 3 examples: a, b, c
  //
  // hard-code-test the three examples as a, b, c and inspect each invocation in gdb
  int k = 3; 
  int returnSize = 0;; 

  // 3rd example is simp[lest test case, so tyry it first:
  int c_arrivalSize = 3; int c_arrival[] = { 1,2,3 };
  int c_loadSize = 3; int c_load[] = { 10,12,11 };
  int* c_busy_list = busiestServers(k, &c_arrival[0], c_arrivalSize, &c_load[0], c_loadSize, &returnSize);
  printf("3rd example results:\n");
  for(int i = 0; i < returnSize; ++i) { printf("busiest server_idx: %d\n", c_busy_list[i]); }

  int b_arrivalSize = 4; int b_arrival[] = { 1,2,3,4 };
  int b_loadSize = 4; int b_load[] = { 1,2,1,2 };
  int* b_busy_list = busiestServers(k, &b_arrival[0], b_arrivalSize, &b_load[0], b_loadSize, &returnSize);
  printf("2nd example results:\n");
  for(int i = 0; i < returnSize; ++i) { printf("busiest server_idx: %d\n", b_busy_list[i]); }

  int a_arrivalSize = 5; int a_arrival[] = { 1,2,3,4,5 };
  int a_loadSize = 5; int a_load[] = { 5,2,3,3,3 };
  printf("1st example results:\n");
  int* a_busy_list = busiestServers(k, &a_arrival[0], a_arrivalSize, &a_load[0], a_loadSize, &returnSize);
  for(int i = 0; i < returnSize; ++i) { printf("busiest server_idx: %d\n", a_busy_list[i]); }
}
